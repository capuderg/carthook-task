<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nba extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
        });

        Schema::create('player', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('team_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');

            $table->index(['first_name', 'last_name']);

            $table->foreign('team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');
        });

        Schema::create('game', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date');
            $table->bigInteger('home_team_id')->unsigned();
            $table->bigInteger('away_team_id')->unsigned();

            $table->index(['date', 'home_team_id', 'away_team_id']);

            $table->foreign('home_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');

            $table->foreign('away_team_id')
                ->references('id')->on('team')
                ->onDelete('cascade');
        });

        Schema::create('period', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('game_id')->unsigned();
            $table->string('period_type'); // regular, overtime ?
            $table->integer('period_number'); // first period, second, ...
            $table->integer('home_score');
            $table->integer('away_score');

            $table->foreign('game_id')
                ->references('id')->on('game')
                ->onDelete('cascade');
        });

        Schema::create('statistic', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('player_id')->unsigned();
            $table->bigInteger('game_id')->unsigned();
            $table->integer('points');
            $table->integer('assists');
            $table->integer('rebounds');
            $table->integer('ft_made');
            $table->integer('ft_attempts');
            $table->integer('3p_made');
            $table->integer('3p_attempts');

            $table->foreign('player_id')
                ->references('id')->on('player')
                ->onDelete('cascade');

            $table->foreign('game_id')
                ->references('id')->on('game')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player');
        Schema::dropIfExists('team');
        Schema::dropIfExists('game');
        Schema::dropIfExists('period');
        Schema::dropIfExists('statistic');
    }
}
