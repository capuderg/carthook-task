<?php

namespace App\Console;

class CsvParser
{
    /**
     * @var string The path to the CSV file.
     */
    protected $filePath;

    /**
     * @var resource A file pointer resource handler.
     */
    protected $fileHandler;

    /**
     * @var array The associative array of the row headers.
     */
    public $dataHeader;

    /**
     * CsvParser constructor.
     *
     * @param string $filePath The path to the CSV file.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;

        $this->setFileHandler();
        $this->parseHeader();
    }

    /**
     * Set the file handler.
     */
    protected function setFileHandler()
    {
        $this->fileHandler = fopen($this->filePath, 'r');
    }

    /**
     * Close the file handler.
     */
    public function closeFileHandler()
    {
        fclose($this->fileHandler);
    }

    /**
     * Set the row header data.
     *
     * @param array $data
     */
    protected function setDataHeader(array $data)
    {
        $this->dataHeader = array_map( 'strip_tags', $data);
    }

    /**
     * Sanitize the data row.
     *
     * @param array $rowData The raw row data.
     * @return array
     */
    private function sanitizeDataRow($rowData)
    {
        $sanitizedRow = [];

        foreach ($rowData as $index => $value) {
            $sanitizedRow[$this->dataHeader[$index]] = strip_tags($value);
        }

        return $sanitizedRow;
    }

    /**
     * Parse the header of the file.
     */
    protected function parseHeader()
    {
        $this->setDataHeader(fgetcsv($this->fileHandler));
    }

    /**
     * Parse the next X lines of the CSV file.
     *
     * @param int $numberOfLines The number of lines to parse.
     * @return array
     */
    public function parseNext(int $numberOfLines)
    {
        $data = [];

        for ($i = 0; $i < $numberOfLines; $i++) {
            $line = fgetcsv($this->fileHandler);

            if (!empty($line)) {
                $data[] = $this->sanitizeDataRow($line);
            } elseif (!$this->hasMoreLines()) {
                break;
            }
        }

        return $data;
    }

    /**
     * Check if the file has more lines to parse.
     *
     * @return bool
     */
    public function hasMoreLines() {
        return !feof($this->fileHandler);
    }
}

