<?php

namespace App\Console;


class Functions
{
    /**
     * -------------------------------------
     * ------------- Task C ----------------
     * -------------------------------------
     */

    /**
     * @var array "best" sorting network for 11 elements from:
     * http://jgamble.ripco.net/cgi-bin/nw.cgi?inputs=11&algorithm=best&output=text
     */
    private static $sortingNetwork11 = [
        [[0,1],[2,3],[4,5],[6,7],[8,9]],
        [[1,3],[5,7],[0,2],[4,6],[8,10]],
        [[1,2],[5,6],[9,10],[0,4],[3,7]],
        [[1,5],[6,10],[4,8]],
        [[5,9],[2,6],[0,4],[3,8]],
        [[1,5],[6,10],[2,3],[8,9]],
        [[1,4],[7,10],[3,5],[6,8]],
        [[2,4],[7,9],[5,6]],
        [[3,4],[7,8]]
    ];

    public static function sort11(array $array)
    {
        foreach (self::$sortingNetwork11 as $row) {
            foreach ($row as $swapArray) {
                self::swap($array, $swapArray[0], $swapArray[1]);
            }
        }

        return $array;
    }

    private static function swap(&$array, $a, $b)
    {
        if ($array[$b] < $array[$a]) {
            $tmp = $array[$a];
            $array[$a] = $array[$b];
            $array[$b] = $tmp;
        }
    }

    public static function insertionSort($array)
    {
        for ($i = 0; $i < count($array); $i++) {
            $val = $array[$i];
            $j = $i-1;
            while($j >= 0 && $array[$j] > $val){
                $array[$j+1] = $array[$j];
                $j--;
            }
            $array[$j+1] = $val;
        }

        return $array;
    }

    public static function timeSortNTimes(int $n, array $array, $algorithm) {
        $start = microtime(true);

        for ($i = 0; $i < $n; $i++) {
            call_user_func_array($algorithm, [&$array]);
        }

        return microtime(true) - $start;
    }

    //    public static function sort11(array $array)
//    {
//        self::swap5($array, self::$sortingNetwork11[0]);
//        self::swap5($array, self::$sortingNetwork11[1]);
//        self::swap5($array, self::$sortingNetwork11[2]);
//        self::swap3($array, self::$sortingNetwork11[3]);
//        self::swap4($array, self::$sortingNetwork11[4]);
//        self::swap4($array, self::$sortingNetwork11[5]);
//        self::swap4($array, self::$sortingNetwork11[6]);
//        self::swap3($array, self::$sortingNetwork11[7]);
//        self::swap2($array, self::$sortingNetwork11[8]);
//
//        return $array;
//    }
//
//    private static function swap5(&$array, $swapArray)
//    {
//        self::swap($array, $swapArray[0][0], $swapArray[0][1]);
//        self::swap($array, $swapArray[1][0], $swapArray[1][1]);
//        self::swap($array, $swapArray[2][0], $swapArray[2][1]);
//        self::swap($array, $swapArray[3][0], $swapArray[3][1]);
//        self::swap($array, $swapArray[4][0], $swapArray[4][1]);
//    }
//
//    private static function swap4(&$array, $swapArray)
//    {
//        self::swap($array, $swapArray[0][0], $swapArray[0][1]);
//        self::swap($array, $swapArray[1][0], $swapArray[1][1]);
//        self::swap($array, $swapArray[2][0], $swapArray[2][1]);
//        self::swap($array, $swapArray[3][0], $swapArray[3][1]);
//    }
//
//    private static function swap3(&$array, $swapArray)
//    {
//        self::swap($array, $swapArray[0][0], $swapArray[0][1]);
//        self::swap($array, $swapArray[1][0], $swapArray[1][1]);
//        self::swap($array, $swapArray[2][0], $swapArray[2][1]);
//    }
//
//    private static function swap2(&$array, $swapArray)
//    {
//        self::swap($array, $swapArray[0][0], $swapArray[0][1]);
//        self::swap($array, $swapArray[1][0], $swapArray[1][1]);
//    }

//    private static function randNArray($n)
//    {
//        $range = range(0, 100);
//        shuffle($range );
//        return array_slice($range ,0, $n);
//    }






    /**
     * -------------------------------------
     * ------------- Task D ----------------
     * -------------------------------------
     */

    public static function sortExponents($array)
    {
        usort($array, function ($a, $b) {
            list($aBase, $aExpo) = array_map('intval', explode('^', $a));
            list($bBase, $bExpo) = array_map('intval', explode('^', $b));

            $aResult = $aExpo * log10($aBase);
            $bResult = $bExpo * log10($bBase);

            if ($aResult == $bResult) {
                return 0;
            }

            return ($aResult < $bResult) ? -1 : 1;
        });

        return $array;
    }

    public static function timeSortExponentsWithNElements(int $n) {
        $array = self::randomExponentsArray($n);

        $start = microtime(true);

        self::sortExponents($array);

        return microtime(true) - $start;
    }

    public static function randomExponentsArray(int $n) {
        $result = [];

        for ($i = 0; $i < $n; $i++) {
            $result[] = sprintf('%1$s^%2$s', rand(100, 10000), rand(100, 10000));
        }

        return $result;
    }
}
