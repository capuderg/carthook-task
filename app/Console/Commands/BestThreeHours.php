<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestThreeHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bestThreeHours
                            {merchant_id : The ID of the merchant}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '3 hour time period for specific merchant when he sells most (Example: 3:00pm to 6:00pm)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $merchant_id = $this->argument('merchant_id');

        $result = DB::table('hourly_merchants')
            ->select(DB::raw('hour(analytics_date) as hour'), DB::raw('sum(sales_total) as total'))
            ->where('merchant_id', '=', $merchant_id)
            ->groupBy('hour')
            ->orderBy('hour')
            ->get()
            ->keyBy('hour')
            ->toArray();

        $sums = [];

        for ($i = 0; $i < 22; $i++) {
            $sum = array_sum(array_column($this->retrieveThreeHours($result, $i), 'total'));

            $sums[] = $sum;
        }

        $maxStart = array_keys($sums, max($sums))[0];

        $date = new Carbon();
        $date->setTime($maxStart, 0);
        $startHour = $date->toTimeString();
        $date->addHours(2);
        $endHour = $date->toTimeString();

        $this->info(sprintf(
            'The best 3 hour selling period for merchant with ID: %1$s is: %2$s - %3$s!',
            $merchant_id,
            $startHour,
            $endHour
        ));
    }

    private function retrieveThreeHours($results, $start)
    {
        return [
            $this->returnArray($results, $start),
            $this->returnArray($results, $start+1),
            $this->returnArray($results, $start+2),
        ];
    }

    private function returnArray($array, $key)
    {
        if (!array_key_exists($key, $array)) {
            return [];
        }

        return $array[$key];
    }
}
