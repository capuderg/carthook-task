<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestSellingMerchant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bestSellingMerchant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'best selling merchant of all the merchants (use sales_total column)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = DB::table('daily_merchants')
            ->join('merchants', 'daily_merchants.merchant_id', '=', 'merchants.merchant_id')
            ->select('merchants.merchant_id', 'merchants.first_name', 'merchants.last_name', DB::raw('sum(daily_merchants.sales_total) as total'))
            ->groupBy('merchants.merchant_id')
            ->orderBy('total', 'desc')
            ->first();

        if (empty($result)) {
            $this->error('No results found!');
        } else {
            $this->info(sprintf(
                'The best selling merchant is: %1$s %2$s (id: %3$s), with %4$s in total sales!',
                $result->first_name,
                $result->last_name,
                $result->merchant_id,
                $result->total
            ));
        }
    }
}
