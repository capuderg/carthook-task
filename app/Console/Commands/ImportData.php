<?php

namespace App\Console\Commands;

use App\Console\CsvParser;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportData extends Command
{
    const PARALLEL_IMPORTS = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the data from app.periscopedata.com links provided in the task.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $imports = [
            [
                'url' => 'https://app.periscopedata.com/api/carthook/chart/csv/d5345630-811c-cd5a-b3e2-c4a6ac1dae1a/265769',
                'table' => 'daily_merchants'
            ],
            [
                'url' => 'https://app.periscopedata.com/api/carthook/chart/csv/b3bb3bbd-0ea3-8234-64bd-3cb474631c30/284541',
                'table' => 'daily_funnels'
            ],
            [
                'url' => 'https://app.periscopedata.com/api/carthook/chart/csv/5ab06803-29bf-76b2-6a5a-ad7cf4b7fc21/284541',
                'table' => 'hourly_merchants'
            ],
            [
                'url' => 'https://app.periscopedata.com/api/carthook/chart/csv/b5798a66-e694-a429-cc5e-2f9e163f6438/284541',
                'table' => 'hourly_funnels'
            ],
        ];

        $this->info('Import started!');

        foreach ($imports as $item) {
            $this->import($item['url'], $item['table']);
            $this->info(sprintf('Table %s finished importing!', $item['table']));
        }

        $this->info('All imports finished! Now generating names for merchants...');

        $this->populateMerchants();

        $this->info('Merchants name generation is complete!');

        $this->info('All done!');
    }

    private function import($csvUrl, $table)
    {
        $dailyMerchantsCsvParser = new CsvParser($csvUrl);

        while ($dailyMerchantsCsvParser->hasMoreLines()) {
            $data = $dailyMerchantsCsvParser->parseNext(self::PARALLEL_IMPORTS);

            $data = array_map(function ($row) use ($dailyMerchantsCsvParser) {
                return array_combine($dailyMerchantsCsvParser->dataHeader, $row);
            }, $data);

            DB::table($table)->insert($data);
        }
    }

    private function populateMerchants() {
        $merchants = DB::table('daily_merchants')->select('merchant_id')->distinct()->get()->toArray();
        $faker = Factory::create();

        $merchants = array_map(function ($item) use ($faker) {
            $item->first_name = $faker->firstName;
            $item->last_name = $faker->lastName;

            return (array)$item;
        }, $merchants);

        $merchantsCollection = collect($merchants);

        $chunks = $merchantsCollection->chunk(self::PARALLEL_IMPORTS);

        foreach ($chunks as $chunk) {
            DB::table('merchants')->insert($chunk->toArray());
        }
    }
}
