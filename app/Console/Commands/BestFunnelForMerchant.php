<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestFunnelForMerchant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bestFunnelForMerchant
                            {merchant_id : The ID of the merchant}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Best performing funnel for a specific merchant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $merchant_id = $this->argument('merchant_id');

        $result = DB::table('daily_funnels')
            ->select('funnel_id', DB::raw('sum(sales_total) as total'))
            ->where('merchant_id', '=', $merchant_id)
            ->groupBy('funnel_id')
            ->orderBy('total', 'desc')
            ->first();

        if (empty($result)) {
            $this->error('No results found!');
        } else {
            $this->info(sprintf(
                'The best performing funnel for merchant with ID: %1$s is: %2$s, with %3$s in total sales!',
                $merchant_id,
                $result->funnel_id,
                $result->total
            ));
        }
    }
}
