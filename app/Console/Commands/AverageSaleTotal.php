<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AverageSaleTotal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:averageSaleTotal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'gets an AVG sales_total per merchant per day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = DB::table('hourly_merchants')
            ->select(DB::raw('merchant_id, avg(sales_total) as avg_sales, DATE(analytics_date) day'))
            ->groupBy('merchant_id', 'day')
            ->orderBy('merchant_id')
            ->get()
            ->toArray();

        $result = array_map(function ($row) {
            return (array)$row;
        }, $result);

        if (empty($result)) {
            $this->error('No results found!');
        } else {
            $this->info('The result of the query is:');
            $this->table(['merchant_id', 'avg_sales', 'day'], $result);
        }
    }
}
