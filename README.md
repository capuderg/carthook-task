# CartHook Senior Backend tasks

The tasks can be found here:
https://gist.github.com/zenkkor/ac3c0c2a671d2a7bd1997663fc59b1ee

## My solutions

### Basic CS

**A)**

I was not sure, if I should "design" the DB scheme in a "SQL DB designer app" or if I should code it out in Laravel (a migration).
Since it says in the tasks note, to use Laravel if possible, I decided to write a Laravel migration schema. The file can be found here:

 database/migrations/2019_06_21_115423_nba.php
 
 I added all the table definitions in one file for easier review.
 
 Resoning:
 
 - I've created the 3 DB tables described in the task (player, team, game).
 - I've added 2 additional tables (period and statistic)
    - a game has 4 regular periods and potentially infinite overtime periods (this is marked with `period_type` and `period_number` columns)
    - the statistic table will hold the data of players for each game
 - The `game` table has an index of 'date', 'home_team_id', 'away_team_id', so that the often used query of searching by date and team name can be performed faster
 - The `player` table has an index of 'first_name', 'last_name', so the search by name can be faster.
 
 ---------
 
 **B)**
 
 Since the title of this task section is "basic CS" I understood the instructions to refer to a terminal/console command to accomplish this task. I'm using macOS, so I would use this command:
 
 `find ./test/ -type f -name '0aH*' -delete`
 
 where the `./test/` is the passed folder path. 
 
 ---------
 
 **C)**
 
 I know quicksort is usually a go-to sorting algorithm, but since we know how many elements we need to sort, I knew there must me something better :)
 
 So I googled a bit and stumbeled across sorting networks and a site that generates a "best" sorting networking for your provided input:
 
 http://jgamble.ripco.net/cgi-bin/nw.cgi?inputs=11&algorithm=best&output=text
 
 I've made a basic implementation of sorting networks with the function `sort11`. I've tested it with comparison of the PHP build-in `sort` function.
 The PHP `sort` (which uses quicksort, was much faster).
 
 I've attempted to simplify my `sort11` function in hopes to speed it up, by not using foreach loops (code commented out). But this did not improve it at all.
 I think the issue is, that PHP can't do parallel processing. If it could execute more commands at the same time, it would probably be faster the quicksort.
 Because sorting network for 11 elements has 35 comparisons and they can be performed in parallel in 9 operations. Quick sort has an average time complexity of n*log(n)
 and a worst case n^2. So, between 38 and 121 comparisons for our 11 element array.
 
 I've also read, that insertion sort is good for small array sorts, so I made a basic implementation of it, but timewise, it was not close to the PHP sort implementation.
 
 The functions are in the `app/Functions.php` file.
 
 Some test result screenshots are in: 
 
 - public/images/C-reverse-order.png
 - public/images/C-reverse-order.png

So I guess, I would just use the built in PHP `sort` function for PHP. In programming language, which can process commands in parallel, I would use the sorting network.

So my estimation for 10^10 executions of PHP sort on a "normal" machine (macbook pro 2014), would be ~3000 seconds.

---------
 
 **D)**
 
 I knew straight away, that calculating the exponents is out of the question, since those are way to big numbers to store in variables.
 
 Again I googled what is the best way to compare two exponents without calculating them and I saw an answer in using logarithms.
 
 So a uncalculatable a^b is transformed to log(a^b) -> b * log(a).
 
 I used these calculated values to compare and sort them using the PHP `usort` function with a custom sorting comparison callback.
 
 The functions are in the `app/Functions.php file at the end of the file.
 
 The function sorts 10000 powers in ~0.15 seconds on my macbook pro 2014.

----------

### Advanced/Practical

I'll be using Laravel (with homestead) and it's console commands for executing the tasks.

Steps for setup the environment:

1. `composer install`
2. `vagrant up`
3. `php artisan migrate`, to initialize the DB tables.

 **A)**
 
I've written a CsvParser wrapper class, which is responsible for opening the remote CSV file and parsing the data in chunks.

The first console command is: `php artisan db:import`, which imports the data from the urls into the DB tables.

I've decided to read the file in 10 line chunks and import those 10 lines in the DB table, but probably a more optimal number can be researched, for DB insert speed optimization.

**A-1)**

The function is located in `App\Console\Commands\ImportData::populateMerchants` and is hooked after the data is imported with the `php artisan db:import` command.

-----------

**A-2)**

1. *best selling merchant* command can be executed with `php artisan command:bestSellingMerchant`
2. *best performing funnel for a specific merchant* command can be executed with `php artisan command:bestFunnelForMerchant merchant_id`, where the `merchant_id` is the ID of the merchant you want to find the best perfomring funnel for.
3. *3 hour time period for specific merchant* command can be executed with `php artisan command:bestThreeHours merchant_id`, where the `merchant_id` is the ID of the merchant you want to find the best selling 3 hour period for.
4. *get an AVG sales_total per merchant per day* command can be executed with `php artisan command:averageSaleTotal`

The one page high-level overview can be found here:

https://docs.google.com/document/d/1YLCiqOUAtCpAnuMVfsayVacY03_EJ7cu7ebs29qyrcY/edit
